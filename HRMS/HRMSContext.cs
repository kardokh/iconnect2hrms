﻿using IconnectUpdateData.HRMS;
using Microsoft.EntityFrameworkCore; 

namespace IconnectUpdateData.HRMS
{
    class HRMSContext : DbContext
    { 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-SCN3FR8\\SQLEXPRESS;Database=Korek;user id=sa; password=sa;MultipleActiveResultSets=true");
        }


        public DbSet<Branch> Branches { get; set; } 
        public DbSet<Ethnicity> Ethnicities { get; set; } 
        public DbSet<Gender> Genders { get; set; } 
        public DbSet<Grade> Grades { get; set; } 
        public DbSet<Group> Groups { get; set; } 
        public DbSet<JobPosition> JobPositions { get; set; } 
        public DbSet<JobTitle> JobTitles { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<MaritalStatus> MaritalStatuses { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Religion> Religions { get; set; }
        public DbSet<IconnectEmployee> IconnectEmployees { get; set; }
        public DbSet<IconnectEmployeeResult> IconnectEmployeeResult { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeStatus> EmployeeStatuses { get; set; }
        public DbSet<IconnectEmployeeJob> IconnectEmployeeJobs { get; set; }
        public DbSet<JobStatus> JobStatuses { get; set; }
        public DbSet<JobType> JobTypes { get; set; }
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<EmploymentType> EmploymentTypes { get; set; }
        public DbSet<EmployeeJob> EmployeeJobs { get; set; }
        public DbSet<IconnectEmployeeJobResult> IconnectEmployeeJobResult { get; set; }
     
    }
}
