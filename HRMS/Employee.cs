﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IconnectUpdateData.HRMS
{
    [Table("Employees", Schema = "HR")]
    class Employee
    {
        public Guid Id { get; set; }
        public int? EmployeeId { get; set; }
        public string Image { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FirstNameArabic { get; set; }
        public string MiddleNameArabic { get; set; }
        public string LastNameArabic { get; set; }
        public string FullNameArabic { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public Guid? BloodGroupId { get; set; }
        public Guid? MaritalStatusId { get; set; }
        public Guid? GenderId { get; set; }
        public Guid? ReligionId { get; set; }
        public Guid? NationalityId { get; set; }
        public Guid? EthnicityId { get; set; }
        public DateTime? HireDate { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string PhoneNo2 { get; set; }
        public string Email2 { get; set; }
        public string IPPhone { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? EmployeeStatusId { get; set; }

    }
}
