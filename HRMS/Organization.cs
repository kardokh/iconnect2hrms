using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("Organizations", Schema = "HR")]
    public class Organization
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? OrganizationTypeId { get; set; }
        public short? Sort { get; set; }
    }
}
