﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IconnectUpdateData.HRMS
{
    public class IconnectEmployeeJobResult
    {
        public Guid Id { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? SupervisorId { get; set; }
        public Guid? BranchId { get; set; }
        public Guid? OrganizationtId { get; set; }
        public Guid? UnitId { get; set; }
        public Guid? JobTitleId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? GradeId { get; set; }
        public Guid? GroupId { get; set; }
        public DateTime? PositionAssignDate { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractExpiryDate { get; set; }
        public Guid? ContractTypeId { get; set; }
        public Guid? JobTypeId { get; set; }
        public Guid? EmploymentTypeId { get; set; }
        public Guid? StatusId { get; set; }
        public DateTime? ProbationStartDate { get; set; }
        public DateTime? ProbationEndDate { get; set; } 
        public decimal? Salary { get; set; } 
    }

}
