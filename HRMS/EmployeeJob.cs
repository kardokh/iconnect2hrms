﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IconnectUpdateData.HRMS
{
    [Table("EmployeeJobs", Schema = "HR")]
    class EmployeeJob
    {
        public Guid Id { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? SupervisorId { get; set; }
        public Guid? BranchId { get; set; }
        public Guid? OrganizationtId { get; set; }
        public Guid? UnitId { get; set; }
        public Guid? JobTitleId { get; set; }
        public Guid? JobPositionId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? EmploymentTypeId { get; set; }
        public Guid? JobStatusId { get; set; }
        public Guid? JobTypeId { get; set; }
        public Guid? GradeId { get; set; }
        public Guid? GroupId { get; set; }
        public Guid? StepId { get; set; }
        public Guid? ContractTypeId { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractExpiryDate { get; set; }
        public DateTime? ProbationStartDate { get; set; }
        public DateTime? ProbationEndDate { get; set; }
        public DateTime? PositionAssignDate { get; set; }
        public decimal? Salary { get; set; }
        public decimal? TotalEarnings { get; set; }
        public decimal? TotalDeductions { get; set; }
        public decimal? NetSalary { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsCurrent { get; set; }

    }
}
