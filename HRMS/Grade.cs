using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("Grades", Schema = "HR")]
    public class Grade
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short? Sort { get; set; }
    }
}
