using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("Genders", Schema = "HR")]
    public class Gender
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short? Sort { get; set; }
    }
}
