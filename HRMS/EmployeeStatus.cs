using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("EmployeeStatuses", Schema = "HR")]
    public class EmployeeStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short? Sort { get; set; }
    }
}
