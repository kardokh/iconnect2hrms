using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("JobTypes", Schema = "HR")]
    public class JobType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short? Sort { get; set; }
    }
}
