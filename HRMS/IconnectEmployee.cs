﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IconnectUpdateData.HRMS
{ 
    public class IconnectEmployeeResult
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public Guid? MaritalStatusId { get; set; }
        public Guid? EmployeeStatusId { get; set; }
        public Guid? GenderId { get; set; }
        public Guid? ReligionId { get; set; }
        public Guid? NationalityId { get; set; }
        public Guid? EthnicityId { get; set; }
        public string HireDate { get; set; }
        public string IPPhone { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
    }

}
