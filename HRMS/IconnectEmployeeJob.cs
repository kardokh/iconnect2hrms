﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IconnectUpdateData.HRMS
{
    [Table("IconnectEmployeeJobs", Schema = "HR")]
    public class IconnectEmployeeJob
    {
        public Guid Id { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? SupervisorId { get; set; }
        public Guid? BranchId { get; set; }
        public Guid? OrganizationtId { get; set; }
        public Guid? UnitId { get; set; }
        public Guid? JobTitleId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? GradeId { get; set; }
        public Guid? GroupId { get; set; }
        public DateTime? PositionAssignDate { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractExpiryDate { get; set; }
        public Guid? ContractTypeId { get; set; }
        public string JobType { get; set; }
        public string Status { get; set; }
        public string EmploymentType { get; set; }
        public DateTime? ProbationStartDate { get; set; }
        public DateTime? ProbationEndDate { get; set; }
        public decimal? Salary { get; set; }

    }

}
