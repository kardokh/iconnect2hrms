using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IconnectUpdateData.HRMS
{
    [Table("Nationalities", Schema = "HR")]
    public class Nationality
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public short? Sort { get; set; }
    }
}
