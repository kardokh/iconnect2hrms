﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_employee_contract_type")]
    public class ContractType
    {
        [Key]
        [Column("Ident")]
        public int Id { get; set; }

        [Column("Contract_Type_Description")]
        public string Name { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
