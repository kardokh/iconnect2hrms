﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_job_classification")]
    public class Job
    {
        [Key]
        [Column("Job_ID")]
        public int Id { get; set; }

        [Column("Job_Title")]
        public string Name { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
