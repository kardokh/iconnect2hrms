﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IconnectUpdateData.Iconnect
{
    public class IconnectEmployeeJobResult
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string SupervisorId { get; set; }
        public string BranchId { get; set; }
        public string OrganizationtId { get; set; }
        public string UnitId { get; set; }
        public string JobTitleId { get; set; }
        public string LocationId { get; set; }
        public string GradeId { get; set; }
        public string GroupId { get; set; }
        public string PositionAssignDate { get; set; }
        public string ContractStartDate { get; set; }
        public string ContractExpiryDate { get; set; }
        public string ContractTypeId { get; set; }
        public string JobType { get; set; }
        public string EmploymentType { get; set; }
        public string Status { get; set; }
        public string ProbationStartDate { get; set; }
        public string Salary { get; set; }
        public string ProbationEndDate { get; set; }
    }

}
