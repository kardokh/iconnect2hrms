﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_employee_job_position")]
    public class EmployeeJob
    {
        [Key]
        [Column("Ident")]
        public int Id { get; set; }

        [Column("Employee_Id")]
        public string EmployeeId { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
