﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace IconnectUpdateData.Iconnect
{
    class IconnectContext : DbContext
    { 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=10.10.8.25;port=3306;database=interact;uid=kardo;password=2746291@Y");
        }


        public DbSet<Employee> Employees { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<EmployeeJob> EmployeeJobs { get; set; }
        public DbSet<IconnectEmployee> IconnectEmployees { get; set; }
        public DbSet<IconnectEmployeeJobResult> IconnectEmployeeJobResult { get; set; }

    }
}
