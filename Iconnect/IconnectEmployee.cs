﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IconnectUpdateData.Iconnect
{
    public class IconnectEmployee
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public string Ethnicity { get; set; }
        public string HireDate { get; set; }
        public string IPPhone { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
    }

}
