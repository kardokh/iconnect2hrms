﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_organization_unit")]
    public class Organization
    {
        [Key]
        [Column("Organization_Unit_ID")]
        public int Id { get; set; }

        [Column("Organization_Unit_Name")]
        public string Name { get; set; }

        [Column("Organization_Unit_Reporting_to_ID")]
        public string ParentId { get; set; }         
        
        [Column("CLASSIFICATION_TYPE")]
        public string Type { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
