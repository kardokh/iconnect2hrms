﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_grade")]
    public class Grade
    {
        [Key]
        [Column("Ident")]
        public int Id { get; set; }

        [Column("Grade_Desc")]
        public string Name { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
