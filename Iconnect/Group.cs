﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_employee_group")]
    public class Group
    {
        [Key]
        [Column("Ident")]
        public int Id { get; set; }

        [Column("Employee_Group_Description")]
        public string Name { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
