﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_work_locations")]
    public class Location
    {
        [Key]
        [Column("IDENT")]
        public int Id { get; set; }

        [Column("LOCATION_NAME")]
        public string Name { get; set; } 

        [Column("UUID")]
        public string UUID { get; set; } 

    }
}
