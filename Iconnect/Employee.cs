﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IconnectUpdateData.Iconnect
{
    [Table("interact_employee")]
    public class Employee
    {
        [Key]
        [Column("Employee_ID")]
        public int Id { get; set; }

        [Column("Employee_Key_ID")]
        public string Code { get; set; }

        [Column("First_Name")]
        public string FirstName { get; set; }

        [Column("Last_Name")]
        public string LastName { get; set; }
        
        [Column("Marital_Status")]
        public string MaritalStatus { get; set; }

        [Column("Religion")]
        public string Religion { get; set; }
        
        [Column("Nationality")]
        public string Nationality { get; set; }
        
        [Column("Gender")]
        public string Gender { get; set; }
        
        [Column("Ethnicity")]
        public string Ethnicity { get; set; }

        [Column("ApplicantPhoto")]
        public string Photo { get; set; }

        [Column("Emp_Status")]
        public string Status { get; set; }

        [Column("UUID")]
        public string UUID { get; set; }

        public string ApplicantCV { get; set; }

    }
}
