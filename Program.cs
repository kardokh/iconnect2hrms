﻿using IconnectUpdateData.HRMS;
using IconnectUpdateData.Iconnect;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Bcpg;
using System;
using System.Linq;

namespace IconnectUpdateData
{
    class Program
    {
        static void Main(string[] args)
        {
            UpdateIconnect();
            UpdateEmployee();
            UpdateEmployeeJob();

            Console.WriteLine("Hello World!");
        }

        static void UpdateEmployee()
        {
            var iconnectContext = new IconnectContext();
            var hrmsContext = new HRMSContext();

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.MaritalStatuses");

            var employees = iconnectContext.Employees.ToList();

            var maritalStatuses = employees.
                Where(c => c.MaritalStatus != null && c.MaritalStatus != "")
                .GroupBy(c => c.MaritalStatus.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            short sort = 1;
            foreach (var maritalStatus in maritalStatuses)
            {
                var _maritalStatus = new MaritalStatus();
                _maritalStatus.Name = FirstCharToUpper(maritalStatus.Name);
                _maritalStatus.Sort = sort;
                hrmsContext.MaritalStatuses.Add(_maritalStatus);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Religions");

            var religions = employees.
                Where(c => c.Religion != null && c.Religion != "")
                .GroupBy(c => c.Religion.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var religion in religions)
            {
                var _religion = new Religion();
                _religion.Name = FirstCharToUpper(religion.Name);
                _religion.Sort = sort;
                hrmsContext.Religions.Add(_religion);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Nationalities");

            var nationalities = employees.
                Where(c => c.Nationality != null && c.Nationality != "")
                .GroupBy(c => c.Nationality.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var nationality in nationalities)
            {
                var _nationality = new Nationality();
                _nationality.Name = FirstCharToUpper(nationality.Name);
                _nationality.Sort = sort;
                hrmsContext.Nationalities.Add(_nationality);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Genders");

            var genders = employees.
                Where(c => c.Gender != null && c.Gender != "")
                .GroupBy(c => c.Gender.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var gender in genders)
            {
                var _gender = new Gender();
                _gender.Name = FirstCharToUpper(gender.Name);
                _gender.Sort = sort;
                hrmsContext.Genders.Add(_gender);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Ethnicities");

            var ethnicities = employees.
                Where(c => c.Ethnicity != null && c.Ethnicity != "")
                .GroupBy(c => c.Ethnicity.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var ethnicity in ethnicities)
            {
                var _ethnicity = new Ethnicity();
                _ethnicity.Name = FirstCharToUpper(ethnicity.Name);
                _ethnicity.Sort = sort;
                hrmsContext.Ethnicities.Add(_ethnicity);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.EmployeeStatuses");

            var employeeStatuses = employees.
                Where(c => c.Status != null && c.Status != "")
                .GroupBy(c => c.Status.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var employeeStatuse in employeeStatuses)
            {
                var _employeeStatus = new EmployeeStatus();
                _employeeStatus.Name = FirstCharToUpper(employeeStatuse.Name);
                _employeeStatus.Sort = sort;
                hrmsContext.EmployeeStatuses.Add(_employeeStatus);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [HR].[IconnectEmployees]");

            var iconnectEmployees = iconnectContext.IconnectEmployees.FromSqlRaw("CALL GetIconnectEmployees()").ToList();


            foreach (var iconnectEmployee in iconnectEmployees)
            {
                var employee = new HRMS.IconnectEmployee();

                employee.Id = Guid.Parse(iconnectEmployee.Id);
                employee.Code = iconnectEmployee.Code;
                employee.Image = iconnectEmployee.Image;
                employee.EmployeeId = iconnectEmployee.EmployeeId;
                employee.FullName = iconnectEmployee.FullName;
                employee.FirstName = iconnectEmployee.FirstName;
                employee.LastName = iconnectEmployee.LastName;
                employee.DateOfBirth = iconnectEmployee.DateOfBirth;
                employee.PlaceOfBirth = iconnectEmployee.PlaceOfBirth;
                employee.MaritalStatus = iconnectEmployee.MaritalStatus;
                employee.Gender = iconnectEmployee.Gender;
                employee.Religion = iconnectEmployee.Religion;
                employee.Nationality = iconnectEmployee.Nationality;
                employee.Ethnicity = iconnectEmployee.Ethnicity;
                employee.HireDate = iconnectEmployee.HireDate;
                employee.IPPhone = iconnectEmployee.IPPhone;
                employee.PhoneNo = iconnectEmployee.PhoneNo;
                employee.Email = iconnectEmployee.Email;
                employee.Status = iconnectEmployee.Status;
                hrmsContext.IconnectEmployees.Add(employee);
            }

            hrmsContext.SaveChanges();

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [HR].[Employees]");

            var _iconnectEmployees = hrmsContext.IconnectEmployeeResult.FromSqlRaw("Exec HR.GetIconnectEmployees").ToList();
            DateTime temp;

            foreach (var iconnectEmployee in _iconnectEmployees)
            {
                var employee = new HRMS.Employee();
                employee.Id = iconnectEmployee.Id;
                employee.EmployeeId = int.Parse(iconnectEmployee.EmployeeId);
                employee.Code = iconnectEmployee.Code;
                employee.Image = iconnectEmployee.Image;
                employee.FullName = iconnectEmployee.FullName;
                employee.FirstName = iconnectEmployee.FirstName;
                employee.LastName = iconnectEmployee.LastName;

                if (DateTime.TryParse(iconnectEmployee.DateOfBirth, out temp))
                {
                    employee.DateOfBirth = temp;
                }

                employee.PlaceOfBirth = iconnectEmployee.PlaceOfBirth;
                employee.MaritalStatusId = iconnectEmployee.MaritalStatusId;
                employee.EmployeeStatusId = iconnectEmployee.EmployeeStatusId;
                employee.GenderId = iconnectEmployee.GenderId;
                employee.ReligionId = iconnectEmployee.ReligionId;
                employee.NationalityId = iconnectEmployee.NationalityId;
                employee.EthnicityId = iconnectEmployee.EthnicityId;

                if (DateTime.TryParse(iconnectEmployee.HireDate, out temp))
                {
                    employee.HireDate = temp;
                }

                employee.IPPhone = iconnectEmployee.IPPhone;
                employee.PhoneNo = iconnectEmployee.PhoneNo;
                employee.Email = iconnectEmployee.Email;
                hrmsContext.Employees.Add(employee);
            }

            hrmsContext.SaveChanges();

        }

        static void UpdateEmployeeJob()
        {

            var iconnectContext = new IconnectContext();
            var hrmsContext = new HRMSContext();
            DateTime temp;
            Guid guidTemp;
            Decimal decimalTemp;
            short sort = 1;

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Organizations");
            var organizations = iconnectContext.Organizations.Where(c => c.Type != "BRANCH").ToList();
            foreach (var organization in organizations)
            {
                var _organization = new HRMS.Organization();
                _organization.Id = Guid.Parse(organization.UUID);
                _organization.Name = organization.Name;
                _organization.Sort = sort;
                hrmsContext.Organizations.Add(_organization);
                sort++;
            }

            sort = 1;
            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Branches");
            var branches = iconnectContext.Organizations.Where(c => c.Type == "BRANCH").ToList();
            foreach (var branch in branches)
            {
                var _branch = new HRMS.Branch();
                _branch.Id = Guid.Parse(branch.UUID);
                _branch.Name = branch.Name;
                _branch.Sort = sort;
                hrmsContext.Branches.Add(_branch);
                sort++;
            }

            sort = 1;
            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.JobTitles");
            var jobTitles = iconnectContext.Jobs.ToList();
            foreach (var jobTitle in jobTitles)
            {
                var _jobTitle = new HRMS.JobTitle();
                _jobTitle.Id = Guid.Parse(jobTitle.UUID);
                _jobTitle.Name = jobTitle.Name;
                _jobTitle.Sort = sort;
                hrmsContext.JobTitles.Add(_jobTitle);
                sort++;
            }

            sort = 1;
            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Grades");
            var grades = iconnectContext.Grades.ToList();
            foreach (var grade in grades)
            {
                var _grade = new HRMS.Grade();
                _grade.Id = Guid.Parse(grade.UUID);
                _grade.Name = grade.Name;
                _grade.Sort = sort;
                hrmsContext.Grades.Add(_grade);
                sort++;
            }

            sort = 1;
            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Groups");
            var groups = iconnectContext.Groups.ToList();
            foreach (var group in groups)
            {
                var _group = new HRMS.Group();
                _group.Id = Guid.Parse(group.UUID);
                _group.Name = group.Name;
                _group.Sort = sort;
                hrmsContext.Groups.Add(_group);
                sort++;
            }
             
            sort = 1;
            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.ContractTypes");
            var contractTypes = iconnectContext.ContractTypes.ToList();
            foreach (var contractType in contractTypes)
            {
                var _contractType = new HRMS.ContractType();
                _contractType.Id = Guid.Parse(contractType.UUID);
                _contractType.Name = contractType.Name;
                _contractType.Sort = sort;
                hrmsContext.ContractTypes.Add(_contractType);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.Locations");
            var locations = iconnectContext.Locations.ToList();
            foreach (var location in locations)
            {
                var _location = new HRMS.Location();
                _location.Id = Guid.Parse(location.UUID);
                _location.Name = location.Name;
                _location.Sort = sort;
                hrmsContext.Locations.Add(_location);
                sort++;
            }

            var employeeJobs = iconnectContext.IconnectEmployeeJobResult.FromSqlRaw("CALL GetIconnectEmployeeJobs").ToList();

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.JobStatuses");

            var jobStatuses = employeeJobs.
                Where(c => c.Status != null && c.Status != "")
                .GroupBy(c => c.Status.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var jobStatus in jobStatuses)
            {
                var _jobStatus = new JobStatus();
                _jobStatus.Name = FirstCharToUpper(jobStatus.Name);
                _jobStatus.Sort = sort;
                hrmsContext.JobStatuses.Add(_jobStatus);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.JobTypes");

            var jobTypes = employeeJobs.
                Where(c => c.JobType != null && c.JobType != "")
                .GroupBy(c => c.JobType.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var jobType in jobTypes)
            {
                var _jobType = new JobType();
                _jobType.Name = FirstCharToUpper(jobType.Name);
                _jobType.Sort = sort;
                hrmsContext.JobTypes.Add(_jobType);
                sort++;
            }

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE HR.EmploymentTypes");

            var employmentTypes = employeeJobs.
                Where(c => c.EmploymentType != null && c.EmploymentType != "")
                .GroupBy(c => c.EmploymentType.Trim().ToLower()).Select(c => new { Name = c.Key, Total = c.Count() })
                .OrderByDescending(c => c.Total)
                .ToList();

            sort = 1;
            foreach (var employmentType in employmentTypes)
            {
                var _employmentType = new EmploymentType();
                _employmentType.Name = FirstCharToUpper(employmentType.Name);
                _employmentType.Sort = sort;
                hrmsContext.EmploymentTypes.Add(_employmentType);
                sort++;
            }

            


            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [HR].[IconnectEmployeeJobs]");


            foreach (var employeeJob in employeeJobs)
            {
                var _employeeJob = new HRMS.IconnectEmployeeJob();

                _employeeJob.Id = Guid.Parse(employeeJob.Id);
                _employeeJob.EmployeeId = Guid.Parse(employeeJob.EmployeeId);
                if (Guid.TryParse(employeeJob.SupervisorId, out guidTemp)) _employeeJob.SupervisorId = guidTemp;
                if (Guid.TryParse(employeeJob.BranchId, out guidTemp)) _employeeJob.BranchId = guidTemp;
                if (Guid.TryParse(employeeJob.OrganizationtId, out guidTemp)) _employeeJob.OrganizationtId = guidTemp;
                if (Guid.TryParse(employeeJob.UnitId, out guidTemp)) _employeeJob.UnitId = guidTemp;
                if (Guid.TryParse(employeeJob.JobTitleId, out guidTemp)) _employeeJob.JobTitleId = guidTemp;
                if (Guid.TryParse(employeeJob.LocationId, out guidTemp)) _employeeJob.LocationId = guidTemp;
                if (Guid.TryParse(employeeJob.GradeId, out guidTemp)) _employeeJob.GradeId = guidTemp;
                if (Guid.TryParse(employeeJob.GroupId, out guidTemp)) _employeeJob.GroupId = guidTemp;
                if (Guid.TryParse(employeeJob.ContractTypeId, out guidTemp)) _employeeJob.ContractTypeId = guidTemp;
                if (Decimal.TryParse(employeeJob.Salary, out decimalTemp)) _employeeJob.Salary = decimalTemp;


                if (DateTime.TryParse(employeeJob.PositionAssignDate, out temp))
                    _employeeJob.PositionAssignDate = temp;

                if (DateTime.TryParse(employeeJob.ContractStartDate, out temp))
                    _employeeJob.ContractStartDate = temp;

                if (DateTime.TryParse(employeeJob.ContractExpiryDate, out temp))
                    _employeeJob.ContractExpiryDate = temp;

                _employeeJob.JobType = employeeJob.JobType;
                _employeeJob.Status = employeeJob.Status;
                _employeeJob.EmploymentType = employeeJob.EmploymentType;

                if (DateTime.TryParse(employeeJob.ProbationStartDate, out temp))
                    _employeeJob.ProbationStartDate = temp;

                if (DateTime.TryParse(employeeJob.ProbationEndDate, out temp))
                    _employeeJob.ProbationEndDate = temp;

                hrmsContext.IconnectEmployeeJobs.Add(_employeeJob);
            }

            hrmsContext.SaveChanges();

            var iconnectEmployeeJobResult = hrmsContext.IconnectEmployeeJobResult.FromSqlRaw("EXEC [HR].[GetIconnectEmployeeJobs]").ToList();

            hrmsContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [HR].[EmployeeJobs]");


            foreach (var employeeJob in iconnectEmployeeJobResult)
            {
                var _employeeJob = new HRMS.EmployeeJob();

                _employeeJob.Id = employeeJob.Id;
                _employeeJob.EmployeeId = employeeJob.EmployeeId;
                _employeeJob.SupervisorId = employeeJob.SupervisorId;
                _employeeJob.BranchId = employeeJob.BranchId;
                _employeeJob.OrganizationtId = employeeJob.OrganizationtId;
                _employeeJob.UnitId = employeeJob.UnitId;
                _employeeJob.JobTitleId = employeeJob.JobTitleId;
                _employeeJob.LocationId = employeeJob.LocationId;
                _employeeJob.GradeId = employeeJob.GradeId;
                _employeeJob.GroupId = employeeJob.GroupId;
                _employeeJob.ContractTypeId = employeeJob.ContractTypeId;
                _employeeJob.PositionAssignDate = employeeJob.PositionAssignDate;
                _employeeJob.ContractStartDate = employeeJob.ContractStartDate;
                _employeeJob.ContractExpiryDate = employeeJob.ContractExpiryDate;
                _employeeJob.JobTypeId = employeeJob.JobTypeId;
                _employeeJob.JobStatusId = employeeJob.StatusId;
                _employeeJob.EmploymentTypeId = employeeJob.EmploymentTypeId;
                _employeeJob.ProbationStartDate = employeeJob.ProbationStartDate;
                _employeeJob.ProbationEndDate = employeeJob.ProbationEndDate;
                _employeeJob.Salary = employeeJob.Salary; 
                hrmsContext.EmployeeJobs.Add(_employeeJob);
            }



            hrmsContext.SaveChanges();


        }

        static void UpdateIconnect()
        {
            var context = new IconnectContext();

            var employees = context.Employees.Where(c => c.UUID == null).ToList();
            foreach (var employee in employees)
            {
                employee.UUID = Guid.NewGuid().ToString();
            }

            var organizations = context.Organizations.Where(c => c.UUID == null).ToList();
            foreach (var organization in organizations)
            {
                organization.UUID = Guid.NewGuid().ToString();
            }

            var jobs = context.Jobs.Where(c => c.UUID == null).ToList();
            foreach (var job in jobs)
            {
                job.UUID = Guid.NewGuid().ToString();
            }

            var grades = context.Grades.Where(c => c.UUID == null).ToList();
            foreach (var grade in grades)
            {
                grade.UUID = Guid.NewGuid().ToString();
            }

            var locations = context.Locations.Where(c => c.UUID == null).ToList();
            foreach (var location in locations)
            {
                location.UUID = Guid.NewGuid().ToString();
            }

            var contractTypes = context.ContractTypes.Where(c => c.UUID == null).ToList();
            foreach (var contractType in contractTypes)
            {
                contractType.UUID = Guid.NewGuid().ToString();
            }

            var groups = context.Groups.Where(c => c.UUID == null).ToList();
            foreach (var group in groups)
            {
                group.UUID = Guid.NewGuid().ToString();
            }

            var employeeJobs = context.EmployeeJobs.Where(c => c.UUID == null).ToList();
            foreach (var employeeJob in employeeJobs)
            {
                employeeJob.UUID = Guid.NewGuid().ToString();
            }

            context.SaveChanges();
        }

        static string FirstCharToUpper(string input)
        {
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}
